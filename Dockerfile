FROM alpine:3.7
LABEL maintainer="Stefan Schönberger <mail@sniner.net>"

ENV OZWCP_COMMIT bbbd461c5763faab4949b12da12901f2d6f00f48
ENV OZW_COMMIT f174f4b3a3ba35cc1a8d74bd71c521de6b428a60

RUN echo '@testing http://nl.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

RUN apk update && \
    apk --no-cache add \
        build-base linux-headers util-linux bash ca-certificates git \
        eudev-dev eudev-libs libmicrohttpd-dev hidapi-dev@testing \
    && \
    mkdir -p /srv && cd /srv && \
    git clone https://github.com/OpenZWave/open-zwave.git && \
    git clone https://github.com/OpenZWave/open-zwave-control-panel.git && \
    cd /srv/open-zwave && \
    git reset --hard ${OZW_COMMIT} && \
    make && \
    cd /srv/open-zwave-control-panel && \
    git reset --hard ${OZWCP_COMMIT} && \
    sed -i '/# for Linux/,+3{/# for Linux/ n; s/^#//}' Makefile && \
    sed -i '/# for Mac/,+5{/# for Mac/ n; s/^\([^#]\)/#\1/}' Makefile && \
    make \
    && \
    apk del build-base git util-linux linux-headers && \
    rm -rf /var/cache/apk/*

COPY ./run.sh /
RUN chmod +x /run.sh

VOLUME /etc/openzwave

EXPOSE 80

CMD ["/run.sh"]
