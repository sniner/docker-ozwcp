# 'OpenZWave Control Panel' for Docker

I expected to need [OZWCP][1] for my Home-Assistant installation, hence I created a Docker image. In the end, I figured I didn't need it. **So first of all a warning: I have never used this image, it may not work as expected.**

Configuration files for OpenZWave are expected in `/etc/openzwave`. For Home-Assistant I assume the same volume/directory has to be mounted on `/config` (Home-Assistant) and `/etc/openzwave` (OZWCP).

`Dockerfile` is based on Alpine, while `Dockerfile.debian` is based on Debian Stretch. Both images are not optimized for size.

[1]: https://github.com/OpenZWave/open-zwave-control-panel
